################################################################################
#                                                                              #
#                ||\  |||| |||| |||\   ||\  /||\  /\  |||\ |||\                #
#                |  | |    |    |  |   |  | |  | |  | |  | |  |                #
#                |  | |    |    |  |   |  | |  | |  | |  | |  |                #
#                |||  |||  |||  |||/   |||  |  | |||| |||  |  |                #
#                |  | |    |    |      |  | |  | |  | |  \ |  |                #
#                |  | |    |    |      |  | |  | |  | |  | |  |                #
#                ||/  |||| |||| |      ||/  \||/ |  | |  | |||/                #
#                                                                              #
################################################################################
# The Beep Board is an IRC bot that reads messages sent in a channel and then  #
# plays relevant sound files or clips.                                         #
#                                                                              #
# View read.md for a full description.                                         #
#                                                                              #
################################################################################
use IRC::Client;
use Beep-Tag::Parser;

class Beep-Bot {
################################################################################
#                              CLASS VARIABLES                                 #
################################################################################
    has $.irc-ip                 = "10.0.0.14";
    has $.irc-channel            = "#general";
    has $.irc-nick               = "Beep-Bot";
    has $!parser                 = Beep-Tag::Parser::Beep-Tag-Parser.new;
    has $!sounds-path            = %*ENV<BEEP_BOT_SOUNDS_PATH> //
                                   "/home/pi/Beep-Board/.sounds/" ~ "/";
    has $!file-extension         = '.ogg';
    has $!play-back-program      = 'ogg123';
    has $!text-to-speech-program = 'espeak';
    has $!text-to-speech-flags   = '';

    # May need to add '-d pulse' or
    # some other flag here to get it
    # to play multiple sounds at once.
    # This is system dependent and works
    # fine without it on raspbian.
    has $!play-back-flags   = '-q';

    # Put a lock around full-halt-state
    # and its relevant subroutine to prevent
    # multiple instances of it running
    # at once.
    has $!full-halt-state     = False;
    has $!full-halt-lock      = Lock.new;

    # Have a lock to count the number of
    # running promises.
    has $!number-of-promises  = 0;
    has $!promise-number-lock = Lock.new;


################################################################################
#                      SUBMETHOD: HALT-PLAY-BACK                               #
################################################################################
# Terminates the playback of the current sound clip by killing all instances   #
# of $!play-back-program and $!text-to-speech-program.                         #
#                                                                              #
# NOTE: This does not terminate all tags in a single IRC message but any audio #
#       that is currently being played.                                        #
#                                                                              #
################################################################################
    submethod halt-play-back () {
        qqx{ killall $!play-back-program };
        qqx{ killall $!text-to-speech-program };

        return;
    }

################################################################################
#                    SUBMETHOD: FULL-HALT-PLAY-BACK                            #
################################################################################
# Terminates the playback of the current sound clip by killing all instances   #
# of $!play-back-program and $!text-to-speech-program and further prevents the #
# execution of any other tags that may have been included in that sound clip's #
# IRC message.                                                                 #
#                                                                              #
# NOTE: This may also terminate some tags sent immediately after the full halt #
#       playback tag.                                                          #
#                                                                              #
################################################################################
    submethod full-halt-play-back () {

        # Prevent multiple occurences of full halt playback from
        # being executed at once.
        $!full-halt-lock.protect({

            # Let the other promises know
            # to quit responding to tags.
            $!full-halt-state = True;

            # Repeat while we have running promises
            # other than this one.
            # Not too concerned about any
            # race conditions for the number
            # of promises.
            while $!number-of-promises != 1 {

                self.halt-play-back();
                sleep(0.5);
            }

            # Let the other promises know
            # to continue responding to tags.
            $!full-halt-state = False;
        });
    }

################################################################################
#                          SUB: GRAB-GREP-TERMS                                #
################################################################################
# Grabs all the grep terms from a grep beep tag and returns them in an array.  #
#                                                                              #
################################################################################
    sub grab-grep-terms ($beep-tag) {
        my $local-tag = $beep-tag<grep-beep-tag>;

        my @return-value = gather {

            while $local-tag {

                take $local-tag<tag-name>;
                $local-tag = $local-tag<grep-beep-tag>;
            }
        }

        return @return-value;
    }

################################################################################
#                              SUBMETHOD: HELP                                 #
################################################################################
# Sends a list of all files in $!sounds-path to $!irc-channel.                 #
#                                                                              #
# Note: This assumes that only sound files are in the directory, as it will    #
#       print out every file immediately visible.                              #
#                                                                              #
################################################################################
    submethod help ($message) {
        $message.irc.send: :where($!irc-channel) :text("AVAILABLE SOUND FILES:");

        # Get the list of files.
        my @results = split("\n", qqx{ ls -p $!sounds-path | grep -v '/' });

        # Send the list to chat.
        for @results -> $result {

            $message.irc.send: :where($!irc-channel) :text($result);
        }

        return;
    }

################################################################################
#                           SUBMETHOD: HELP-TAGS                               #
################################################################################
# Sends a list of all user beep tags in the bookmark file to $!irc-channel.    #
#                                                                              #
################################################################################
    submethod help-tags ($message) {
        $message.irc.send: :where($!irc-channel) :text("USER-DEFINED TAGS:");

        my $bookmarks-file = $!sounds-path ~ "/.bookmarks";

        for $bookmarks-file.IO.lines -> $line {

            # Grab the tag from each line,
            # and separate it from its command.
            my $tag = split(" ", $line, 2)[0];

            $message.irc.send: :where($!irc-channel) :text($tag);
        }

        return;
    }

################################################################################
#                           SUBMETHOD: TEXT-TO-SPEECH                          #
################################################################################
# Plays the text to speech tag through $!text-to-speech-program.               #
#                                                                              #
################################################################################
    submethod text-to-speech ($beep-tag) {
            qqx{ $!text-to-speech-program $!text-to-speech-flags "$beep-tag<text-to-speech>" };

        return;
    }

################################################################################
#                           SUBMETHOD: PLAY-USER-TAG                           #
################################################################################
# Plays the user beep tag if it exists in "$!sounds-path/.bookmarks". If it    #
# doesn't exist, it plays the next closest tag to it, g.e., ;/thank will play  #
# ;/thank-you if ;/thank doesn't exit and ;/thank-you is the next closest tag  #
# alphabetically.                                                              #
#                                                                              #
################################################################################
    submethod play-user-tag ($beep-tag) {
        my $result = qqx{ look $beep-tag<user-tag-name> $!sounds-path/.bookmarks | head -n 1 };

        # If the tag exists, play it.
        if ($result) {

            # Split the tag from the command.
            my $user-tag = $!sounds-path ~ split(" ", $result, 2)[1];

            qqx{ $!play-back-program $!play-back-flags $user-tag };
        }

        return;
    }

################################################################################
#                         SUBMETHOD: PLAY-GREP-BEEP-TAG                        #
################################################################################
# Plays all sound files that match all of the grep terms provided. The order   #
# they are played in is random.                                                #
#                                                                              #
# NOTE: Since the sound files are executed in a loop here, a check for         #
#       $!full-halt-state must be made and handled.                            #
#                                                                              #
################################################################################
    submethod play-grep-beep-tag ($beep-tag) {
        # Get the search terms.
        my @search-terms = grab-grep-terms($beep-tag);
        my $patterns = "";

        # Transform all the search terms into grep statements.
        for @search-terms -> $term {

            $patterns = $patterns ~ " | grep $term";
        }

        # Get the list of files and shuffle them randomly.
        my @files = split("\n", qqx{ ls -p $!sounds-path | grep -v '/' $patterns | shuf });

        for @files -> $file {

            # Exit the loop if full halt is enabled.
            if $!full-halt-state {

                last;
            }

            # Skip this iteration if $file isn't defined.
            next unless $file;

            my $file-with-path = "$!sounds-path/$file";
            qqx{ $!play-back-program $!play-back-flags $file-with-path };
        }
    }

################################################################################
#                       SUBMETHOD: PLAY-USER-GREP-BEEP-TAG                     #
################################################################################
# Plays all user beep tags that match all of the grep terms provided. The      #
# order they are played in is random.                                          #
#                                                                              #
# NOTE: Since the sound files are executed in a loop here, a check for         #
#       $!full-halt-state must be made and handled.                            #
#                                                                              #
################################################################################
    submethod play-user-grep-beep-tag ($beep-tag) {
        # Get the search terms.
        my @search-terms = grab-grep-terms($beep-tag);
        my $patterns = "";

        # Transform all the search terms into grep statements.
        for @search-terms -> $term {

            $patterns = $patterns ~ " | grep $term";
        }

        # Get the list of tags and shuffle them randomly.
        my @tags = split("\n", qqx{ grep -o '^\\S*' $!sounds-path/.bookmarks $patterns | shuf });

        # Iterate over all matching tags.
        for @tags -> $tag {

            # needed for possible perl6 bug?
            next unless $tag;

            # Exit the loop if full halt is enabled.
            if $!full-halt-state {
                last;
            }

            # Grab the tag's command and play it.
            my $result = qqx{ look $tag $!sounds-path/.bookmarks | head -n 1};
            my $user-tag = $!sounds-path ~ split(" ", $result, 2)[1];

            qqx{ $!play-back-program $!play-back-flags $user-tag } if $user-tag;
        }
    }

################################################################################
#                         SUBMETHOD: PLAY-BEEP-TAG                             #
################################################################################
# Plays the corresponding beep tag. If the sound file doesn't exist, it plays  #
# a random longer sound file that begins the same.                             #
#                                                                              #
################################################################################
    submethod play-beep-tag ($beep-tag) {
        # Use shell here to get exitcode information.
        my $file-exists-p = shell "stat $!sounds-path/$beep-tag<tag-name>$!file-extension";

        # If the file doesn't exist, choose a similar one,
        # with a longer file name.
        when $file-exists-p.exitcode != 0 {

            # Grab a file randomly that has a matching beginning.
            my $file-to-play = $!sounds-path ~ qqx{ ls -1 $!sounds-path | grep ^$beep-tag<tag-name> | shuf -n 1 };

            # Make sure we've actually found a file, otherwise
            # we'll play a bunch of garbage from $!sounds-path.
            qqx{ $!play-back-program $!play-back-flags $file-to-play } unless $file-to-play eq $!sounds-path;

            return;
        }


        my $file-and-options = "$beep-tag<tag-name>" ~ $!file-extension;

        # Position to start at.
        with $beep-tag<start> {

            $file-and-options = $file-and-options ~ " -k " ~ $beep-tag<start>;
        }

        # Position to end at.
        with $beep-tag<end> {

            $file-and-options = $file-and-options ~ " -K " ~ $beep-tag<end>;
        }

        # Save the tag.
        with $beep-tag<book-mark> {

            my $to-look-for = $beep-tag<book-mark> ~ " ";
            my $new-entry = $to-look-for ~ $file-and-options;

            # Make sure the file exists.
            qqx{ touch $!sounds-path/.bookmarks };

            # If it already exists, don't write another line.
            unless (qqx{ look "$to-look-for" $!sounds-path/.bookmarks }) {

                # https://stackoverflow.com/questions/10658380/shell-one-liner-to-add-a-line-to-a-sorted-file
                qqx{ echo $new-entry | sort -o $!sounds-path/.bookmarks -m - $!sounds-path/.bookmarks};
            }
        }

        my $full-file-and-options = $!sounds-path ~ $file-and-options;

        # Play the file if it exists.
        qqx{ $!play-back-program $!play-back-flags $full-file-and-options };

        return;
    }

################################################################################
#                     SUBMETHOD: IRC-PRIVMSG-CHANNEL                           #
################################################################################
# Accepts messages from $.irc-privmsg-channel and grabs all the beep tags from #
# them.                                                                        #
#                                                                              #
# For each message received, a promise will be created and the number of them  #
# is incremented. Then each beep-tag found will be dispatched individually.    #
# Then the beep tag will be sent to the relevant submethod to be taken care    #
# of. Then the number of promises is decremented.                              #
#                                                                              #
# NOTE: Since the sound files are executed in a loop here, a check for         #
#       $!full-halt-state must be made and handled.                            #
#                                                                              #
################################################################################
    submethod irc-privmsg-channel ($message) {
        # Grab the beep tags.
        my @tags = $message.text ~~ m:g/ \;<-[;]>+ /;

        # Continue if there were any tags found.
        with @tags {

            start {
                # increment the number of promises.
                $!promise-number-lock.protect({
                    $!number-of-promises++;
                });

                for @tags -> $tag {
                    # Exit the loop if full halt is enabled.
                    if $!full-halt-state {
                        last;
                    }

                    my $match = $!parser.subparse($tag);

                    # Dispatch.
                    with $match<grep-beep-tag>  {
                        if $match<user-tag> {
                            self.play-user-grep-beep-tag($match);
                        } else {
                            self.play-grep-beep-tag($match);
                        }

                        next;
                    }
                    with $match<tag-name>            { self.play-beep-tag($match); next; }
                    with $match<user-tag-name>       { self.play-user-tag($match); next; }
                    with $match<halt-play-back>      { self.halt-play-back(); next; }
                    with $match<full-halt-play-back> { self.full-halt-play-back(); next; }
                    with $match<help>                { self.help($message); next; }
                    with $match<help-tags>           { self.help-tags($message); next; }
                    with $match<text-to-speech>      { self.text-to-speech($match); next; }
                }

                # Decrement the number of promises.
                $!promise-number-lock.protect({
                    $!number-of-promises--;
                });
            }
        }

        # Suppress unnecessary output.
        return;
    }
}

################################################################################
#                             START THE BOT                                    #
################################################################################

my $beep-bot = Beep-Bot.new;

# Add support for other options from the environment variables.
.run with IRC::Client.new:
    :nick(%*ENV<BEEP_BOT_NICK> // $beep-bot.irc-nick)
    :alias('foo', /b.r/)
    :host(%*ENV<BEEP_BOT_IRC_IP> // $beep-bot.irc-ip)
    :channels(%*ENV<BEEP_BOT_CHANNEL> // $beep-bot.irc-channel)
    :plugins($beep-bot)
