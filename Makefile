run-bot: lib/Beep-Tag/Parser.pm6 Beep-Bot.p6
	perl6 -I lib/ Beep-Bot.p6

test: lib/Beep-Tag/Parser.pm6 t/Parser.t
	prove -e 'perl6 -I lib' t/ -v

test-docs: lib/Beep-Tag/Parser.pm6 t/Parser.t
	perl6 -I lib/ --doc=Text t/Parser.t
