unit module Beep-Tag::Parser;

grammar Beep-Tag-Parser is export {
                # Parse for help.
    token TOP { <tag> [ <help> |
                # Parse for full halt play-back.
                <full-halt-play-back> |
                # Parse for halt play-back.
                <halt-play-back> |
                # Parse for help tags.
                <help-tags> |
                # Parse for user tags.
                <user-tag> <user-tag-name> |
                # Parse for text to speech.
                <quote> <text-to-speech> <quote> |
                # Parse for grep beep tag.
                <grep-beep-tag> |
                # Parse for grep user beep tag.
                <user-tag> <grep-beep-tag> |
                # Parse for beep tags.
                <tag-name> [ '@' [ <start> | '-' <end> | <start> '-' <end> ] [':' <book-mark> ]? ]? ] }

    token tag { ';' }

    token full-halt-play-back { '!!' }

    token halt-play-back { '!' }

    token help { 'help' }

    token help-tags { 'help-tags' }

    token user-tag { '/' }

    token grep-tag { '*' }

    token grep-beep-tag { <grep-tag> <tag-name> <grep-beep-tag> | <grep-tag> <tag-name> }

    token user-tag-name { <[A..Za..z0..9_-]>+ }

    token tag-name { <[A..Za..z0..9_-]>+ }

    token quote { '"' }

    token text-to-speech { <[A..Za..z0..9\ .!?',_-]>+ }

    token start { \d+ [ \. \d+ ]? }

    token end { \d+ [ \. \d+ ]? }

    token book-mark { <[A..Za..z0..9_-]>* }
}
