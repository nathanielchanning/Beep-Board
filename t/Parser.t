use v6;
use Test;

use-ok 'Beep-Tag::Parser';

use Beep-Tag::Parser;

=begin pod
=head1 **Beep-Tag::Parser::Beep-Tag-Parser Tests**
=end pod

# Grab a parser for all the tests.
my $Beep-Tag-Parser = Beep-Tag::Parser::Beep-Tag-Parser.new;


=begin pod
=head2 --Parse Name--

Test that a name can be parsed.
=end pod

subtest 'Parse Name' => {
    my $test-tag = ';name';
    my $match    = $Beep-Tag-Parser.parse($test-tag);

    is 'name', $match<tag-name>;

    # Make sure unused parts don't exist.
    nok $match<start>;
    nok $match<end>;
    nok $match<book-mark>;
}

=begin pod
=head2 --Parse Name, Start--

Test that a name and a starting time
can be parsed.
=end pod

subtest 'Parse Name, Start' => {
    my $test-tag = ';name@9.6';
    my $match    = $Beep-Tag-Parser.parse($test-tag);

    is 'name', $match<tag-name>;
    is '9.6',  $match<start>;

    # Make sure unused parts don't exist.
    nok $match<end>;
    nok $match<book-mark>;
}

=begin pod
=head2 --Parse Name, Start, End--

Test that a name, a starting time, and
an ending time can be parsed.
=end pod

subtest 'Parse Name, Start, End' => {
    my $test-tag = ';name@9.6-10';
    my $match    = $Beep-Tag-Parser.parse($test-tag);

    is 'name', $match<tag-name>;
    is '9.6',  $match<start>;
    is '10',   $match<end>;

    # Make sure unused parts don't exist.
    nok $match<book-mark>;
}

=begin pod
=head2 --Parse Name, End--

Test that a name and
an ending time can be parsed.
=end pod

subtest 'Parse Name, Start, End' => {
    my $test-tag = ';name@-10';
    my $match    = $Beep-Tag-Parser.parse($test-tag);

    is 'name', $match<tag-name>;
    is '10',   $match<end>;

    # make sure a start doesn't exist.
    nok $match<start>;
}

=begin pod
=head2 --Parse Name, Start, Book-Mark--

Test that a name, a starting time, and
a book mark can be parsed.
=end pod

subtest 'Parse Name, Start, Book-Mark' => {
    my $test-tag = ';name@9.6:new-tag';
    my $match    = $Beep-Tag-Parser.parse($test-tag);

    is 'name',    $match<tag-name>;
    is '9.6',     $match<start>;
    is 'new-tag', $match<book-mark>;

    # Make sure unused parts don't exist.
    nok $match<end>;
}

=begin pod
=head2 --Parse Name, Start, End, Book-Mark--

Test that a name, a starting time,
an ending time, and a book mark can be parsed.
=end pod

subtest 'Parse Name, Start, End, Book-Mark' => {
    my $test-tag = ';name@9.6-10:new-tag';
    my $match    = $Beep-Tag-Parser.parse($test-tag);

    is 'name',    $match<tag-name>;
    is '9.6',     $match<start>;
    is '10',      $match<end>;
    is 'new-tag', $match<book-mark>;
}


=begin pod
=head2 --Parse Name, End, Book-Mark--

Test that a name, an ending time, and a book mark
can be parsed.
=end pod

subtest 'Parse Name, End, Book-Mark' => {
    my $test-tag = ';name@-10:new-tag';
    my $match    = $Beep-Tag-Parser.parse($test-tag);

    is 'name',      $match<tag-name>;
    is '10',        $match<end>;
    is 'new-tag',   $match<book-mark>;

    # Make sure unused parts don't exist.
    nok $match<start>;
}

=begin pod
=head2 --Parse Help--

Test that a help tag can be parsed.
=end pod

subtest 'Parse Help' => {
    my $test-tag = ';help';
    my $match = $Beep-Tag-Parser.parse($test-tag);

    ok $match<help>;
}

=begin pod
=head2 --Parse Help-Tags--

Test that a help-tags tag can be parsed.
=end pod

subtest 'Parse Help-Tags' => {
    my $test-tag = ';help-tags';
    my $match = $Beep-Tag-Parser.parse($test-tag);

    ok $match<help-tags>;
}

=begin pod
=head2 --Parse User-Tag--

Test that a user-tag can be parsed.
=end pod

subtest 'Parse User-Tag' => {
    my $test-tag = ';/user-tag';
    my $match = $Beep-Tag-Parser.parse($test-tag);

    ok $match<user-tag>;
}

=begin pod
=head2 --Parse Halt Play-Back--

Test that a halt play-back tag can be parsed.
=end pod

subtest 'Parse Halt Play-Back' => {
    my $test-tag = ';!';
    my $match = $Beep-Tag-Parser.parse($test-tag);

    ok $match<halt-play-back>;
}

=begin pod
=head2 --Parse Text To Speech--

Test that text to speech segments can be parsed.
=end pod

subtest 'Parse Text To Speech' => {
    my $test-tag = ';"Hello, World!"';
    my $match = $Beep-Tag-Parser.parse($test-tag);

    is "Hello, World!", $match<text-to-speech>;
}

=begin pod
=head2 --Parse Grep Beep Tag--

Test that grep beep tag segments can be parsed.
=end pod

subtest 'Parse Grep Beep Tag' => {
    my $test-tag = ";*test*thing";
    my $match = $Beep-Tag-Parser.parse($test-tag);

    is "test",  $match<grep-beep-tag><tag-name>;
    is "thing", $match<grep-beep-tag><grep-beep-tag><tag-name>;
}

=begin pod
=head2 --Parse Grep User Beep Tag--

Test that grep user beep tag segments can be parsed.
=end pod

subtest 'Parse Grep User Beep Tag' => {
    my $test-tag = ";/*test*thing";
    my $match = $Beep-Tag-Parser.parse($test-tag);

    is "test",  $match<grep-beep-tag><tag-name>;
    is "thing", $match<grep-beep-tag><grep-beep-tag><tag-name>;
}

=begin pod
=head2 --Parse Full Halt Play Back--

Test that full halt play back can be parsed.
=end pod

subtest 'Parse Full Halt Play Back' => {
    my $test-tag = ";!!";
    my $match = $Beep-Tag-Parser.parse($test-tag);

    $match.say;

    ok $match<full-halt-play-back>;
}

done-testing;
