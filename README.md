# The Beep Board

![alt text] (beeps.png)

## What is the Beep Board?

The Beep Board is an IRC bot that scans each message for Beep Tags.
The Beep Tags are then parsed and translated into corresponding
ogg vorbis sound clips. It is created to run on a Raspberry Pi
connected to a sound system.

## Beep Tags

Beep Tags are how a user communicates what should be played.
The core functionality is the following:

- Specify a file to play.
- Specify a section of a file to play.
- Save a section of a file to play as a user-defined tag. 
- Specify a user defined tag to play.

| **Example Tag**              | **Explanation**                                                                                                                 |
|:-------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| **;!**                       | Stop all audio that is playing. Note that this does not stop consecutive beep tags that appear in the same IRC message.         |
| **;!!**                      | Stop all audio that is playing and any consecutive beep tags that haven't been played yet.                                      |
| **;help**                    | List the available sound files that are available.                                                                              |
| **;help-tags**               | List all off the user-defined tags that have been made.                                                                         |
| **;tag-name**                | Play the file 'tag-name.ogg'.                                                                                                   |
| **;tag-name@0.5**            | Play the file 'tag-name.ogg' starting at 0.5 seconds in.                                                                        |
| **;tag-name@-5**             | Play the file 'tag-name.ogg' stopping at 5 seconds in.                                                                          |
| **;tag-name@0.5-5**          | Play the file 'tag-name.ogg' starting at 0.5 seconds in and ending 5 seconds in.                                                |
| **;tag-name@0.5-5:user-tag** | Play the file 'tag-name.ogg' starting at 0.5 seconds in and ending 5 seconds in, and save it as a user-defined tag, 'user-tag'. |
| **;/user-tag**               | Play the user-defined tag 'user-tag'.                                                                                           |
| **;"text-to-speech"**        | Say 'text-to-speech' through the text-to-speech program.                                                                        |
| **;\*tag**                   | Play any sound file that has tag in its name.                                                                                   |
| **;\*tag\*name**             | Play any sound file that has both tag and name in its name.                                                                     |
| **;/\*tag**                  | Play any user-defined tag that has tag in its name.                                                                             |
| **;/\*tag\*name**            | Play any user-defined tag that has both tag and name in its name.                                                               |

The full grammar can be found in 'lib/Beep-Tag/Parser.pm6'.

## Setting up the Beep Board

The project has the following dependencies:

- Perl 6
- The Perl 6 module IRC::Client
- ogg123
- espeak (optional for Text To Speech)

Perl 6 can be installed via rakudobrew, IRC::Client can be installed via zef,
and ogg123 and espeak can be installed via apt-get on Raspbian.

Currently, there is some manual set-up that has to be done in 'Beep-Bot.p6'
or through environment variables.
The class members, $.irc-ip, $.irc-channel, $.irc-nick,
and $!sounds-path, all need to be configured manually. 

- $.irc-ip => the ip of the irc server
- $.irc-channel => the IRC channel for the Beep Bot to join.
- $.irc-nick => the name of the Beep Bot.
- $!sounds-path => a path to a directory that holds *only* '.ogg' files.

Environment variables can also be used.

- BEEP\_BOT\_IRC\_IP => the ip of the irc server
- BEEP\_BOT\_CHANNEL => the IRC channel for the Beep Bot to join.
- BEEP\_BOT\_NICK => the name of the Beep Bot.
- BEEP\_BOT\_SOUNDS\_PATH => a path to a directory that holds *only* '.ogg' files.

## Running the Beep Board

The easiest way to get the Beep Board up and running
is to use GNU Screen. Connect to the Pi, and open up
a new screen session. Start the Beep Board, and press
'C-a d' to disconnect the screen.
